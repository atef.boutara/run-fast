﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    public static int level = 1;
    public bool gameWon;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            gameWon = false;
            SceneManager.sceneLoaded += SceneManager_sceneLoaded;
            DontDestroyOnLoad(gameObject);
        }
        else Destroy(gameObject);
    }

    private void SceneManager_sceneLoaded(Scene arg0, LoadSceneMode arg1)
    {
        gameWon = false;
    }

    bool SceneExists(string name)
    {
        if (SceneUtility.GetBuildIndexByScenePath(name) >= 0)
            return true;
        return false;
    }

    public void GoNextLevel()
    {
        level++;
        Debug.Log("Should load scene : GamePlay" + level);
        if(SceneExists("GamePlay" + level))
            SceneManager.LoadScene("GamePlay" + level);
    }

    public void Play()
    {
        SceneManager.LoadScene("GamePlay");
    }
}
