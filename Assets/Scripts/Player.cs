﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

[Serializable]
public class Inventory
{
    public Transform SlotsParent;
    public Sprite EmptySlotSprite;
    [Serializable]
    public class Slot
    {
        public Image slot;
        public string name;
        public string type;
        public bool used = false;
        public GameObject GameObjectToInstantiate = null;

        //Inventory slots
        public Slot(Image slot, string name, string type, GameObject GameObjectToInstantiate = null)
        {
            this.slot = slot;
            this.name = name;
            this.type = type;
            this.GameObjectToInstantiate = GameObjectToInstantiate;
        }
    }
    public Slot[] Slots;

    public void InitializeSlots()
    {
       
        for(int i = 0; i<3; i++)
        {
            Slots[i] = new Slot(SlotsParent.GetChild(i).GetChild(0).GetComponent<Image>(), "", "");
        }
    }
    //Adding item to inventory
    public bool AddItem(Sprite sp, string type, string name, GameObject GameObjectToInstantiate = null)
    {
        for (int i = 0; i < 3; i++)
        {
            if(!Slots[i].used)
            {
                Slots[i].name = name;
                Slots[i].slot.sprite = sp;
                Slots[i].type = type;
                Slots[i].GameObjectToInstantiate = GameObjectToInstantiate;
                Slots[i].used = true;
                return true;
            }
        }
        return false;
    }

    public void ResetSlot(int x)
    {
        Slots[x].name = "";
        Slots[x].slot.sprite = EmptySlotSprite;
        Slots[x].GameObjectToInstantiate = null;
        Slots[x].type = "";
        Slots[x].used = false;
    }
    //powerup use
    public void UseInventorySlot(int x,Player p)
    {
        if (!Slots[x].used)
            return;
        switch(Slots[x].name)
        {
            case "Shield":
                {
                    p.ShieldPlayer();
                    ResetSlot(x);
                    break;
                }
            case "FlamedArrows":
                {
                    p.UseFlameArrow(Slots[x].GameObjectToInstantiate);
                    ResetSlot(x);
                    break;
                }
            case "SprintBoot":
                {
                    p.SprintPlayer();
                    ResetSlot(x);
                    break;
                }
        }
    }
}

public class Player : MonoBehaviour
{
    public GameObject WinPanel;
    public Text winnerName;
    public Slider HealthBar;
    public float MAX_HEALTH = 100;
    public float Health = 100;
    public GameObject OtherPlayer;
    public bool Shielded = false;
    public float jumpForce = 620f;
    public bool CanWallJump = false;
    bool lookingRight = true;

    [SerializeField]
    public Inventory inventory;

    public InputAction Movement;
    public InputAction Hook;
    public InputAction InventoryButton1;
    public InputAction InventoryButton2;
    public InputAction InventoryButton3;
    Rigidbody2D rb2d;
    Vector2 movementVector = Vector2.zero;
    Vector3 hookFPos = Vector3.zero;
    LineRenderer HookLine;
    bool Hooking = false;
    public bool isGrounded;
    Animator anim;
    public float respawnTime = 1f;
    public bool isDead = false;

    [Range(10, 100)]
    public float HookRange = 50f;

    public float speed = 10f;
    public float maxSpeed = 10f;

    public float hookSpeed = 10f;
    GameObject lastTouchedWall = null;
    GameObject currentTouchedWall = null;
    public LayerMask wallLayer;
    public LayerMask groundLayer;
    public Collider2D groundCheck;
    Collider2D playerCollider;
    public Cinemachine.CinemachineVirtualCamera VC;

    private void Awake()
    {
        Movement.performed += onMovement;
        Hook.performed += onHook;
        InventoryButton1.performed += onInventoryButton1;
        InventoryButton2.performed += onInventoryButton2;
        InventoryButton3.performed += onInventoryButton3;
        playerCollider = GetComponent<Collider2D>();
    }

    private void OnEnable()
    {
        Movement.Enable();
        Hook.Enable();
        InventoryButton1.Enable();
        InventoryButton2.Enable();
        InventoryButton3.Enable();
    }

    private void onInventoryButton1(InputAction.CallbackContext inputContext)
    {
        inventory.UseInventorySlot(0, this);
    }

    private void onInventoryButton2(InputAction.CallbackContext inputContext)
    {
        inventory.UseInventorySlot(1, this);
    }

    private void onInventoryButton3(InputAction.CallbackContext inputContext)
    {
        inventory.UseInventorySlot(2, this);
    }

    private void onMovement(InputAction.CallbackContext inputContext)
    {
        if (isDead || GameManager.Instance.gameWon)
            return;
        Vector2 movementDir = inputContext.ReadValue<Vector2>();

        anim.SetBool("Running", true);
        movementVector = movementDir;
        if (movementDir.y > 0 && (isGrounded || (CanWallJump && lastTouchedWall != currentTouchedWall)))
            rb2d.AddForce(transform.up * jumpForce);
    }
    //
    private void onHook(InputAction.CallbackContext inputContext)
    {
        /*if (!anim.GetBool("Running"))
            return;*/
        if (GameManager.Instance.gameWon)
            return;
        Debug.Log("Player " + transform.name + " hooking");
        Vector3 checkDir = (movementVector.y != 0 ? transform.right * movementVector.x + transform.up * movementVector.y : transform.right * movementVector.x);

        RaycastHit2D hitInfo = Physics2D.Raycast(transform.position, checkDir, HookRange, LayerMask.GetMask("Ground"));
        if (hitInfo.collider)
        {
            hookFPos = hitInfo.point;
            Hooking = true;
            Debug.Log("Can hook (will touch " + hitInfo.transform.name + ")");
            HookLine.SetPosition(0, Vector3.zero);
            HookLine.SetPosition(1, Vector3.zero);
            HookLine.SetPosition(0, transform.position);
            HookLine.SetPosition(1, hookFPos);
        }
        else
            Debug.Log("No raycast for hook");
        Debug.Log(hookFPos);
    }
    private void Start()
    {
        GameManager.Instance.gameWon = false;

        Health = MAX_HEALTH;
        HealthBar.value = Health / 100;
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        HookLine = transform.GetChild(0).GetComponent<LineRenderer>();

        InitlializeInventory();
    }
    //Player damage
    public void HitPlayer(float damage)
    {
        Health -= damage;
        if (Health <= 0)
        {
            Health = 0;
            StartCoroutine(respawnPlayer());
        }
        HealthBar.value = Health / 100;
    }
    //Player respawn
    IEnumerator respawnPlayer()
    {
        SpriteRenderer sp = GetComponent<SpriteRenderer>();
        isDead = true;
        sp.color = Color.red;
        yield return new WaitForSeconds(respawnTime);
        Health = 100f;
        HealthBar.value = Health / 100;
        sp.color = Color.white;
        isDead = false;
    }

    void InitlializeInventory()
    {
        inventory.InitializeSlots();
    }

    void CheckResetMovement()
    {
        if (Movement.WasReleasedThisFrame())
        {
            movementVector = Vector2.zero;
            anim.SetBool("Running", false);
        }
        if (Hook.WasReleasedThisFrame() || !GameManager.Instance.gameWon && Hooking && hookFPos.x < transform.position.x)
        {
            Debug.Log("Resetting hook");
            hookFPos = Vector3.zero;
            Hooking = false;
            HookLine.SetPosition(0, Vector3.zero);
            HookLine.SetPosition(1, Vector3.zero);
        }

    }

    void UpdateHooks()
    {
        HookLine.SetPosition(0, transform.position);
        HookLine.SetPosition(1, hookFPos);
        Vector2 dir = hookFPos - transform.position;
        transform.Translate(dir * hookSpeed * Time.deltaTime);
    }

    void Flip()
    {
        lookingRight ^= true;
        transform.localScale = new Vector3(-transform.localScale.x, transform.localScale.y, transform.localScale.z);
        Cinemachine.CinemachineFramingTransposer cft = VC.GetCinemachineComponent<Cinemachine.CinemachineFramingTransposer>();
        cft.m_TrackedObjectOffset.x = -cft.m_TrackedObjectOffset.x;
    }

    void Update()
    {
        isGrounded = groundCheck.IsTouchingLayers(groundLayer);
        CanWallJump = playerCollider.IsTouchingLayers(wallLayer);
        if (lookingRight && movementVector.x < 0 || !lookingRight && movementVector.x > 0)
            Flip();
        CheckResetMovement();
        if (isDead)
            return;
        if (Hooking)
            UpdateHooks();
        if (!GameManager.Instance.gameWon)
            transform.Translate(Vector2.right * movementVector.x * speed * Time.deltaTime);
    }

    //Defensive powerup : Sprint timer
    IEnumerator speedTime()
    {
        yield return new WaitForSeconds(5);
        speed = 30f;
    }
    //Defensive powerup : Shield timer
    IEnumerator shieldTime()
    {
        yield return new WaitForSeconds(5);
        transform.GetChild(1).gameObject.SetActive(false);
        Shielded = false;
    }
    

    //Defensive powerup : Sprint
    public void SprintPlayer()
    {
        speed = maxSpeed;
        StartCoroutine(speedTime());
    }
        //Defensive powerup : Shield
    public void ShieldPlayer()
    {
        transform.GetChild(1).gameObject.SetActive(true);
        Shielded = true;
        StartCoroutine(shieldTime());
    }
    //Offesnive powerup : Arrows
    public void UseFlameArrow(GameObject prefab)
    {
        GameObject flame = Instantiate(prefab);
        flame.transform.localScale = new Vector3((transform.position.x > OtherPlayer.transform.position.x ? -3 : 3), 3, 3);
        flame.GetComponent<OffensiveItem>().target = OtherPlayer;
        flame.transform.position = transform.position + (transform.forward * 5f);
    }

    //replay button
    public void Replay()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "VerticalBoost")
        {
            collision.gameObject.GetComponent<Animator>().SetTrigger("Pressed");
            rb2d.AddForce(2500f * transform.up);
        }
        else if (collision.transform.tag == "WinCondition")
        {
            WinPanel.SetActive(true);
            winnerName.text = transform.name;
            GameManager.Instance.gameWon = true;
        }
        else if (!Shielded && collision.transform.tag == "Obstacle")
        {
            HitPlayer(100);
        }

    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.layer == wallLayer)
        {
            lastTouchedWall = currentTouchedWall;
            currentTouchedWall = collision.gameObject;
            
        }

    }

    public void LeaveGame()
    {
        Application.Quit();
    }
    
    public void NextButton()
    {
        WinPanel.SetActive(false);
        GameManager.Instance.GoNextLevel();
    }
}

