﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OffensiveItem : MonoBehaviour
{
    public string _name;
    public GameObject target;

    Rigidbody2D rb;
    public float speed = 20f;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void RotateTowardsTarget()
    {
        var offset = 90f;
        Vector2 direction = target.transform.position - transform.position;
        direction.Normalize();
        float angle = Mathf.Atan2(direction.y, direction.x) * Mathf.Rad2Deg;
        transform.rotation = Quaternion.Euler(Vector3.forward * (angle + offset));
    }

    void Update()
    {
        if(_name == "FlamedArrows")
        {
            if (Vector2.Distance(transform.position, target.transform.position) < 2)
            {
                Player tp = target.GetComponent<Player>();
                if(!tp.Shielded)
                    tp.HitPlayer(30f);
                Destroy(gameObject);
            }
            transform.rotation = Quaternion.Euler(new Vector3(transform.rotation.eulerAngles.x, transform.rotation.eulerAngles.y, transform.position.x > target.transform.position.x ? 45 : -45));
            Vector3 dir = new Vector2(target.transform.position.x, target.transform.position.y) - new Vector2(transform.position.x,transform.position.y);
            rb.velocity = speed * dir;
        }
    }
}
