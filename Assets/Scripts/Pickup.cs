﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Pickup : MonoBehaviour
{
    public Sprite sprite;
    public string type;
    public string _name;
    public GameObject GameObjectToInstantiate  = null;

    private void Start()
    {
        sprite = GetComponent<SpriteRenderer>().sprite;
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            bool added = collision.gameObject.GetComponent<Player>().inventory.AddItem(sprite, type, _name, GameObjectToInstantiate);
            if(added)
                Destroy(gameObject);
        }
    }
}
